﻿function pageLoad() {
    if (Eventos.length == 0) {
        mostraPlaceholder();
    }
    else {
        var temActive = false;
        Eventos.forEach(function (item, index) {
            if (temActive == false && item.ativo == true) {
                temActive = true;
            }
        });
        if (temActive) {
            LoadGastos();
        }
        else {
            Eventos[0].ativo = true;
            LoadGastos();
        }
    }
}

window.onload = pageLoad;

function LoadGastos() {
    LoadPremium();
    LoadNavbar();
    LoadUsers();
    LoadEvento();
    LoadLista();
    LoadHistorico();
    LoadTotalHistorico();
    mostraEvento();
    
    $('[data-toggle="tooltip"]').tooltip();
}

//---------------------------NAVBAR---------------------------------------


var descricao = "";
var custo = 0;
var user = "";
$('.nav.navbar-nav').on('click', 'li:not(.disabled)', function () {
    mostraEvento();
    
    var idEvento = parseInt($(this).attr('id'));
    Eventos.forEach(function (item, index) {
        item.ativo = false;
        if (item.id === idEvento) {
            item.ativo = true;
            eventoActive = item;
        }
    });
    LoadGastos();
    $('.navbar-toggle').click();
});

$('#btnAddEvento').click(function () {
    LoadNewEvento();
    mostraAddEvento();
    LoadSugestoes();
    
    $('.navbar-toggle').click();
});
//------------------------END NAVBAR-------------------------------------

//---------------------------USERS---------------------------------------
function LoadUsers() {
    $('.container-user img:not(:last-child)').each(function () {
        $(this).remove();
    });
    eventoActive.membros.forEach(function (membro, index) {

        var html = '<img src="images/' + search(membro, Membros).icon + '" data-toggle="tooltip" data-placement="bottom" title="' + search(membro, Membros).nome + '" />';
        $('.container-user').prepend(html);
    });
    $('[data-toggle="tooltip"]').tooltip();
}

function AddUser(obj) {
    eventoActive.membros.push(obj.id);
}
//---------------------------END USERS------------------------------------


//---------------------------EVENTO---------------------------------------
function LoadEvento() {
    $('#imgEvento').attr('src', 'images/' + eventoActive.imagem);
    document.getElementById('lblEventoNome').innerText = eventoActive.nome;
    document.getElementById('lblEventoData').innerText = eventoActive.data;
}
//---------------------------END EVENTO------------------------------------

//---------------------------LISTA DE COMPRAS------------------------------

function LoadLista() {
    $('.container-gasto .row:not(:first-child):not(.gasto-fixed)').each(function () {
        $(this).remove();
    });
    eventoActive.listaDeCompras.forEach(function (item, index) {
        var html = '<div class="row"> \
                        <div class="col-xs-8" > \
                            <label>' + item.nome + '</label> \
                        </div > \
                        <div class="col-xs-4"> \
                            <input type="number" class="form-control txt-money" placeholder="0000,00" maxlength="10" /> \
                        </div> \
                    </div>';
        $('.container-gasto').append(html);
    });

    $('.txt-money').on('keyup', function (e) {
        if (e.keyCode === 13) {
            this.blur();
            AddGasto(this);
            this.value = '';
            document.getElementById('txtAddCompra').value = '';
        }
    });
    $('.txt-money').on('keydown', function (e) {
        if (e.keyCode === 9) {
            this.blur();
            AddGasto(this);
            this.value = '';
            document.getElementById('txtAddCompra').value = '';
        }
    });
}

//------------------------END LISTA DE COMPRAS------------------------------

//------------------------------HISTORICO-----------------------------------

function LoadHistorico() {
    $('.container-historico .historico-item').each(function () {
        $(this).remove();
    });
    eventoActive.historico.forEach(function (item, index) {
        var img;
        eventoActive.membros.forEach(function (membro, i) {
            if (search(membro, Membros).nome === item.user) {
                img = search(membro, Membros).icon;
            }
        });
        html = '<div id="' + item.id + '" class="row historico-item"> \
                    <div class="col-xs-4" > \
                        <img src="images/' + img + '" class="add" height="40" width="40" /> \
                        <label>' + item.user + '</label> \
                    </div > \
                    <div class="col-xs-5"> \
                        <label class="less-dark">' + item.nome + '</label> \
                    </div> \
                    <div class="col-xs-3"> \
                        <label>' + item.valor + '</label> \
                    </div> \
                </div >';
        $('.container-historico').append(html);
    });

    $('.historico-item').on('click', function () {
        var element = this;
        swal({
            text: 'Deseja mesmo apagar esse item?',
            type: 'error',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(function () {
            result = -1;
            eventoActive.historico.forEach(function (item, index) {
                if (item.id === element.id) result = index;
            });
            eventoActive.historico.splice(result, 1);
            LoadHistorico();
            LoadTotalHistorico();
        });
    });
}

function AddGasto(eve) {
    custo = $(eve).val();
    if (custo === "") {
        document.getElementById("lblError").innerText = "Por favor, informe um valor.";
        return;
    }
    else {
        custo = parseFloat(custo);
        if (custo === "") {
            document.getElementById("lblError").innerText = "valor com formato inválido.";
            return;
        }
    }
    descricao = $(eve).parent().prev().children().text();
    if (descricao === "")
        descricao = $(eve).parent().prev().children().val();

    if (descricao === "") {
        document.getElementById("lblError").innerText = "Por favor, informe uma descrição.";
        return;
    }

    document.getElementById("lblError").innerText = "";
    if (premiumMode) {
        var options = {};
        $.map(Membros,
            function (o) {
                if ($.inArray(o.id, eventoActive.membros) !== -1) {
                    options[o.nome] = o.nome;
                }
            });

        swal({
            title: 'Me Poupe Premium!',
            type: 'info',
            input: 'select',
            inputOptions: options,
            inputPlaceholder: 'Selecione o responsável pelo gasto',
            inputClass: 'form-control',

            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            showCancelButton: true,

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value !== '') {
                        resolve();
                    } else {
                        reject('Escolha um responsável acima!');
                    }
                });
            }
        }).then(function (result) {
            user = result;
            bindGasto();
        });
    }
    else {
        user = "Você";
        setTimeout(function () {
            bindGasto();
        }, 500);
    }
}


function LoadTotalHistorico() {
    var label = document.getElementById("lblTotalHistorico");
    var total = 0;
    eventoActive.historico.forEach(function (item, index) {
        total += parseFloat(item.valor);
    });
    label.innerText = total.toFixed(2);
}

function bindGasto() {

    eventoActive.historico.unshift({ id: "gasto" + (eventoActive.historico.length + 1), user: user, nome: descricao, valor: custo.toFixed(2) });
    LoadHistorico();
    LoadTotalHistorico();
}


//-----------------------------END HISTORICO--------------------------------

//---------------------------MEMBROS DO EVENTO------------------------------

$('#btnAddUser').on('click', function () {

    swal.setDefaults({
        confirmButtonText: 'OK',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        progressSteps: ['1', '2']
    });

    var usuario = '';

    var steps = [
        {
            title: 'Convidar',
            text: 'Quem você deseja convidar ao grupo ' + eventoActive.nome + '?',
            input: 'text',
            preConfirm: function (user) {
                usuario = user;
                return new Promise(function (resolve, reject) {
                    var exists = false;
                    var inEvent = false;
                    $.each(Membros, function (i, obj) {
                        if (obj.nome === user.trim()) {
                            exists = true;
                            $.each(eventoActive.membros, function (i, mem) {
                                if (mem === obj.id) {
                                    inEvent = true;
                                    return false;
                                }
                            });
                            return false;
                        }
                    });
                    if (!exists) reject('Não existe um usuário com esse nome.');
                    else if (inEvent) reject('Esse usuário já participa do evento.');
                    else resolve();
                });
            }
        },
        {
            title: 'Confirmação',
            text: 'Convidar ' + usuario + ' ao grupo ' + eventoActive.nome + '?'
        }
    ];

    swal.queue(steps).then(function (result) {
        swal.resetDefaults();
        swal({
            title: 'Pronto!',
            html: result[0] + ' convidado para o grupo ' + eventoActive.nome + '!',
            confirmButtonText: 'OK'
        });
        var pessoa;
        $.each(Membros, function (i, obj) {
            if (obj.nome === result[0]) {
                pessoa = obj;
                return false;
            }
        });
        AddUser(pessoa);
        LoadUsers();
    }, function () {
        swal.resetDefaults();

    });
});

$('#btnConfirmEvento').click(function () {
    var nome = document.getElementById('txtNomeNewEvento').value;
    var data = document.getElementById('txtDataNewEvento').value;

    if (nome == '' && data == '') {
        document.getElementById('lblErrorTitleEvento').innerHTML = 'Insira um nome e uma data para o evento.';

    }

    else if (nome == '') {
        document.getElementById('lblErrorTitleEvento').innerHTML = 'Insira o nome do evento.';
    }
    else if (data == '') {
        document.getElementById('lblErrorTitleEvento').innerHTML = 'Insira a data do evento.';
    }
    else {
        NewEvento.nome = nome;
        NewEvento.data = data;
        NewEvento.id = Eventos.length + 1;
        if (NewEvento.imagem == '') NewEvento.imagem = 'img_no_icon.png';
        Eventos.push(NewEvento);
        mostraEvento();
        pageLoad();

        
    }
    
});

function LoadNewEvento() {
    document.getElementById('lblErrorTitleEvento').innerHTML = '';
    document.getElementById('lblErrorEvento').innerHTML = '';
    document.getElementById('txtNomeNewEvento').value = '';
    document.getElementById('txtDataNewEvento').value = '';
    document.getElementById('txtAddGasto').value = '';
    document.getElementById('imgEventoAdd').src = 'images/img_no_icon.png';
    $('.container-compras .compra-item').each(function () {
        $(this).remove();
    });
    $('.compra-placeholder').show();
}
$('#btnAddGasto').click(function () {
    var descricao = document.getElementById('txtAddGasto').value;
    if (descricao == '') {
        document.getElementById('lblErrorEvento').innerHTML = 'Por favor, informe uma descrição para o item.';
    }
    else {
        document.getElementById('lblErrorEvento').innerHTML = '';
        var html = '<div class="row compra-item"> \
                        <div class="align-left col-xs-12"> \
                            <label>' + descricao + '</label> \
                        </div> \
                    </div>';
        $('.compra-placeholder').hide();
        $('.container-compras').last().append(html);
        document.getElementById('txtAddGasto').value = '';
        var novo = { nome: descricao };
        NewEvento.listaDeCompras.push(novo);


        $('.compra-item').on('click', function () {
            var element = $(this).children().first().children().first()[0].innerHTML;
            swal({
                text: 'Deseja mesmo apagar esse item?',
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não'
            }).then(function () {
                result = -1;
                NewEvento.listaDeCompras.forEach(function (item, index) {
                    if (item.nome === element) result = index;
                });
                NewEvento.listaDeCompras.splice(result, 1);
                LoadSugestoes();
            });
        });
    }
});

function LoadSugestoes() {
    $('.container-compras .compra-item').each(function () {
        $(this).remove();
    });
    NewEvento.listaDeCompras.forEach(function (item, index) {
        var html = '<div class="row compra-item"> \
                        <div class="align-left col-xs-12"> \
                            <label>' + item.nome + '</label> \
                        </div> \
                    </div>';
        $('.container-compras').last().append(html);
    });

    $('.compra-item').on('click', function () {
        var element = $(this).children().first().children().first()[0].innerHTML;
        swal({
            text: 'Deseja mesmo apagar esse item?',
            type: 'error',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(function () {
            result = -1;
            NewEvento.listaDeCompras.forEach(function (item, index) {
                if (item.nome === element) result = index;
            });
            NewEvento.listaDeCompras.splice(result, 1);
            LoadSugestoes();
        });
    });
    
}

$('#imgEventoAdd').click(function () {
    var options = {};
    options['img-evento-aviao.png'] = 'Avião';
    options['img-evento-casa.png'] = 'Casa';
    options['img-evento-compras.png'] = 'Compras';
    options['img-evento-coracao.png'] = 'Coração';
    options['img-evento-mala.png'] = 'Mala';
    options['img-evento-placas.png'] = 'Placas';
    options['img-evento-temometro.png'] = 'Termômetro';

    swal({
        title: 'Selecione um ícone para o evento!',
        type: 'info',
        input: 'select',
        inputOptions: options,
        inputPlaceholder: 'Selecione uma opção',
        inputClass: 'form-control',

        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value !== '') {
                    resolve();
                } else {
                    reject('Escolha uma opção acima!');
                }
            });
        }
    }).then(function (result) {
        var imgpath = 'images/' + result;
        document.getElementById('imgEventoAdd').src = imgpath;
        NewEvento.imagem = result;
    });
});


$('.container-event').click(function () {
    swal({
        title: 'Deseja ver detalhes sobre os gastos?',
        type: 'question',

        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
    }).then(function (result) {
        if (eventoActive.historico.length > 0) {
            if (eventoActive.membros.length > 1) {
                LoadResumoTitulo();
                LoadResumo();
                LoadTotalResumo();
                mostraResumo();
            }
            else {
                swal({
                    title: 'Chame mais pessoas para o seu evento!',
                    type: 'error',
                    confirmButtonText: 'OK!',
                });
            }
        }
        else {
            swal({
                title: 'Adicione pelo menos um gasto no evento para isso!',
                type: 'error',
                confirmButtonText: 'OK!',
            });
        }

    });

    
});

function LoadResumoTitulo(){
    $('#imgEventoResumo').attr('src', 'images/' + eventoActive.imagem);
    document.getElementById('lblEventoNomeResumo').innerText = eventoActive.nome;
    document.getElementById('lblEventoDataResumo').innerText = eventoActive.data;
}

function LoadResumo() {
    $('.container-resumo .historico-item').each(function () {
        $(this).remove();
    });

    eventoActive.membros.forEach(function (item, index) {
        var pessoa;

        Membros.forEach(function (person, index) {
            if (person.id === item) {
                pessoa = person;
            }
        });

        var soma = 0;
        eventoActive.historico.forEach(function (hist, index) {
            if (hist.user === pessoa.nome) {
                soma += parseFloat(hist.valor);
            }
        });

        var img = pessoa.icon;
        

        html = '<div id="' + item.id + '" class="row historico-item"> \
                    <div class="col-xs-4" > \
                        <img src="images/' + img + '" class="add" height="40" width="40" /> \
                        <label>' + pessoa.nome + '</label> \
                    </div > \
                    <div class="col-xs-5"> \
                    </div> \
                    <div class="col-xs-3"> \
                        <label>' + soma.toFixed(2) + '</label> \
                    </div> \
                </div >';
        $('.container-resumo').append(html);
    });

}

function LoadTotalResumo() {
    var label = document.getElementById("lblTotalHistoricoResumo");
    var total = 0;
    eventoActive.historico.forEach(function (item, index) {
        total += parseFloat(item.valor);
    });
    label.innerText = total.toFixed(2);
}


function LoadDetalhes() {
    $('.container-detalhes .historico-item').each(function () {
        $(this).remove();
    });

    var n = eventoActive.membros.length;

    var total = 0;
    eventoActive.historico.forEach(function (item, index) {
        total += parseFloat(item.valor);
    });

    var valorDividido = total / n;

    eventoActive.membros.forEach(function (item, index) {
        var pessoa;

        Membros.forEach(function (person, index) {
            if (person.id === item) {
                pessoa = person;
            }
        });

        var soma = 0;
        eventoActive.historico.forEach(function (hist, index) {
            if (hist.user === pessoa.nome) {
                soma += parseFloat(hist.valor);
            }
        });
        var concat = "";
        if (soma - valorDividido >= 0) {
            concat = ' class="lbl-green"> +' + (soma - valorDividido).toFixed(2)
        }
        else {
            concat = ' class="lbl-red">' + (soma - valorDividido).toFixed(2)
        }
        html = '<div id="' + item.id + '" class="row historico-item"> \
                    <div class="col-xs-4" > \
                        <img src="images/' + pessoa.icon + '" class="add" height="40" width="40" /> \
                        <label>' + pessoa.nome + '</label> \
                    </div > \
                    <div class="col-xs-5"> \
                        <label' + concat + '</label> \
                    </div> \
                    <div class="col-xs-3"> \
                        <label>' + valorDividido.toFixed(2) + '</label> \
                    </div> \
                </div >';
        $('.container-detalhes').append(html);
    });

}

$('#btnDividir').click(function () {
    LoadDetalhes();
    mostraDetalhes();
})