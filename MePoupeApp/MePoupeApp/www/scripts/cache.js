﻿var premiumMode = false;
var eventoActive;

var Membros =
    [
        { id: 1, nome: "Você", icon: "img_user_1.png" },
        { id: 2, nome: "Will", icon: "img_user_2.png" },
        { id: 3, nome: "Donna", icon: "img_user_3.png" },
        { id: 4, nome: "Pedro", icon: "img_user_2.png" },
        { id: 5, nome: "Lucas", icon: "img_user_3.png" },
        { id: 6, nome: "Gui", icon: "img_user_2.png" },
        { id: 7, nome: "Paula", icon: "img_user_2.png" },
        { id: 8, nome: "Matheus", icon: "img_user_3.png" }
    ];

var NewEvento = {
    membros: [1],
    nome: "",
    imagem: "img_no_icon.png",
    data: "",
    listaDeCompras: [],
    historico: [],
    favorito: false,
    ativo: false
};

var Eventos =
    [
        
    ];

//---------------------------NAVBAR----------------------------------------------------
function LoadNavbar() {
    $('.nav.navbar-nav li:not(:first-child)').each(function () {
        $(this).remove();
    });

    Eventos.forEach(function (item, index) {
        var evento = item;
        var html = '<li id="' + evento.id + '" class="' + (evento.ativo ? 'active' : '') + '"> \
                        <div class="col-xs-10" > \
                            <a href="#">' + evento.nome + '</a> \
                        </div > \
                        <div class="col-xs-2"> \
                            <img class="star" src="' + (evento.favorito ? 'images/star_checked.png' : 'images/star_unchecked.png') + '" height="30" width="30" /> \
                        </div> \
                    </li >';
        $('.nav.navbar-nav:last-child').append(html);
        if (item.ativo) eventoActive = item;

    });
}

$('.nav.navbar-nav').on('click', '.star', function (event) {
    var idEvento = parseInt($(this).parent().parent().attr('id'));
    Eventos.forEach(function (item, index) {
        item.favorito = false;
        if (item.id === idEvento) {
            item.favorito = true;
        }
    });
    LoadNavbar();
    event.stopPropagation();
});

$('.nav.navbar-nav').on('click', 'li:not(.disabled)', function () {
    var idEvento = $(this).attr('id');
    Eventos.forEach(function (item, index) {
        item.ativo = false;
        if (item.id === idEvento) {
            item.ativo = true;
            eventoActive = item;
        }
    });
    LoadGastos();
    $('.navbar-toggle').click();
});

function LoadPremium() {
    if (premiumMode) {
        document.getElementById("lblPremium").innerText = "Premium";
    }
    else {
        document.getElementById("lblPremium").innerText = "";
    }
    $('#lblBrand').click(function (event) {
        if (premiumMode) {
            premiumMode = false;
            document.getElementById("lblPremium").innerText = "";
        }
        else {
            premiumMode = true;
            document.getElementById("lblPremium").innerText = "Premium";
        }
        event.stopImmediatePropagation();
    });
}

//-----------------------------END NAVBAR---------------------------------------


function search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].id === nameKey) {
            return myArray[i];
        }
    }
    return null;
}

function mostraEvento() {
    $('.container-title, .separator-evento, .container-evento, .container-compras, .container-placeholder, .container-resumo-titulo, .container-resumo, .container-button, .container-detalhes, .separator-resumo').fadeOut(400, function () {
        $('.container-user, .container-event, .container-gasto, .separator-gasto, .container-historico').fadeIn();
    });
}
function mostraPlaceholder() {
    $('.container-title, .separator-evento, .container-evento, .container-compras, .container-user, .container-event, .container-gasto, .separator-gasto, .container-historico, .container-resumo-titulo, .container-resumo, .container-button, .container-detalhes, .separator-resumo').hide();
    $('.container-placeholder').show();
}
function mostraAddEvento() {
    $('.container-user, .container-event, .container-gasto, .separator-gasto, .container-historico, .container-placeholder, .container-resumo-titulo, .container-resumo, .container-button, .container-detalhes, .separator-resumo').fadeOut(400, function () {
        $('.container-title, .separator-evento, .container-evento, .container-compras').fadeIn();
    });
    NewEvento = new Object();
    NewEvento.nome = '';
    NewEvento.data = '';
    NewEvento.favorito = false;
    NewEvento.historico = [];
    NewEvento.imagem = 'img_no_icon.png';
    NewEvento.listaDeCompras = [];
    NewEvento.membros = [1];
}

function mostraResumo() {
    $('.container-title, .separator-evento, .container-evento, .container-compras, .container-user, .container-event, .container-gasto, .separator-gasto, .container-historico, .container-placeholder, .container-detalhes, .separator-resumo').fadeOut(400, function () {
        $('.container-resumo-titulo, .container-resumo, .container-button').fadeIn();
    });
    
}

function mostraDetalhes() {
    $('.separator-resumo, .container-detalhes').fadeIn();
}